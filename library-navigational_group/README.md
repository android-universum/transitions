@Transitions-Navigational
===============

This module groups the following modules into one **single group**:

- [Navigational-Base](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-base)
- [Navigational-Framework](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-framework)
- [Navigational-Compat](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-compat)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atransitions/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atransitions/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:transitions-navigational:${DESIRED_VERSION}@aar"
