@Transitions-View
===============

This module groups the following modules into one **single group**:

- [View-Core](https://bitbucket.org/android-universum/transitions/src/main/library-view-core)
- [View-Reveal](https://bitbucket.org/android-universum/transitions/src/main/library-view-reveal)
- [View-Scale](https://bitbucket.org/android-universum/transitions/src/main/library-view-scale)
- [View-Translate](https://bitbucket.org/android-universum/transitions/src/main/library-view-translate)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atransitions/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atransitions/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:transitions-view:${DESIRED_VERSION}@aar"

_depends on:_
[transitions-util](https://bitbucket.org/android-universum/transitions/src/main/library-util)