/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.transition;

import android.os.Build;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class BaseNavigationalTransitionTest extends AndroidTestCase {

	@Test public void testInflateTransition() {
		// Arrange:
		final int transitionResId = context().getResources().getIdentifier(
				"transition_fade",
				"transition",
				context().getPackageName()
		);
		final TestTransition navigationalTransition = new TestTransition();
		// Act:
		final Transition transition = navigationalTransition.inflateTransition(context(), transitionResId);
		// Assert:
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			assertThat(transition, is(not(nullValue())));
			assertThat(transition, instanceOf(Fade.class));
			assertThat(navigationalTransition.inflateTransition(context(), transitionResId), is(not(transition)));
		} else {
			assertThat(transition, is(nullValue()));
		}
	}

	@Test public void testInflateTransitionManager() {
		// Arrange:
		final int transitionResId = context().getResources().getIdentifier(
				"transition_manager",
				"transition",
				context().getPackageName()
		);
		final TestTransition navigationalTransition = new TestTransition();
		final ViewGroup sceneRoot = new FrameLayout(context());
		// Act:
		final TransitionManager transitionManager = navigationalTransition.inflateTransitionManager(context(), transitionResId, sceneRoot);
		// Assert:
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			assertThat(transitionManager, is(not(nullValue())));
			assertThat(navigationalTransition.inflateTransitionManager(context(), transitionResId, sceneRoot), is(not(transitionManager)));
		} else {
			assertThat(transitionManager, is(nullValue()));
		}
	}

	private static final class TestTransition extends BaseNavigationalTransition<TestTransition> {

		TestTransition() { super(); }
	}
}