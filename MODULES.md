Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/transitions/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/transitions/src/main/library-core)**
- **[@Navigational](https://bitbucket.org/android-universum/transitions/src/main/library-navigational_group)**
- **[Navigational-Base](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-base)**
- **[Navigational-Framework](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-framework)**
- **[Navigational-Compat](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-compat)**
- **[@View](https://bitbucket.org/android-universum/transitions/src/main/library-view_group)**
- **[View-Core](https://bitbucket.org/android-universum/transitions/src/main/library-view-core)**
- **[View-Reveal](https://bitbucket.org/android-universum/transitions/src/main/library-view-reveal)**
- **[View-Scale](https://bitbucket.org/android-universum/transitions/src/main/library-view-scale)**
- **[View-Translate](https://bitbucket.org/android-universum/transitions/src/main/library-view-translate)**
- **[@Window](https://bitbucket.org/android-universum/transitions/src/main/library-window_group)**
- **[Window-Core](https://bitbucket.org/android-universum/transitions/src/main/library-window-core)**
- **[Window-Common](https://bitbucket.org/android-universum/transitions/src/main/library-window-common)**
- **[Window-Extra](https://bitbucket.org/android-universum/transitions/src/main/library-window-extra)**
- **[Util](https://bitbucket.org/android-universum/transitions/src/main/library-util)**
