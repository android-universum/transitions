Transitions-Navigational-Compat
===============

This module contains implementation of `BaseNavigationalTransition` that may be used in context
of `Fragment` from the `android.support.v4.app` package.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atransitions/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atransitions/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:transitions-navigational-compat:${DESIRED_VERSION}@aar"

_depends on:_
[transitions-navigational-base](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [NavigationalCompatTransition](https://bitbucket.org/android-universum/transitions/src/main/library-navigational-compat/src/main/java/universum/studios/android/transition/NavigationalCompatTransition.java)
