Transitions-Window-Extra
===============

This module contains some **extra** implementations of `WindowTransition` along with appropriate
animation resources.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Atransitions/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Atransitions/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:transitions-window-extra:${DESIRED_VERSION}@aar"

_depends on:_
[transitions-window-core](https://bitbucket.org/android-universum/transitions/src/main/library-window-core),
[transitions-window-common](https://bitbucket.org/android-universum/transitions/src/main/library-window-common)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [WindowExtraTransitions](https://bitbucket.org/android-universum/transitions/src/main/library-window-extra/src/main/java/universum/studios/android/transition/WindowExtraTransitions.java)